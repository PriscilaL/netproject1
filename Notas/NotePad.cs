﻿using Notas.BaseStream;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Notas
{
    public partial class NotePad : Form
    {
        public NotePad()
        {
            InitializeComponent();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Pad p = new Pad();
            p.MdiParent = this;
            p.Show();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int count = this.MdiChildren.Length;
            if(count == 0)
            {
                return;
            }
            DialogResult result = saveFileDialog1.ShowDialog();
            if( result == DialogResult.OK)
            {
                string filepath = saveFileDialog1.FileName;
                SecuentialStream ss = new SecuentialStream(filepath);
                Form activeChild = this.ActiveMdiChild;
                TextBox txtarea = (TextBox)activeChild.Controls[0];
                ss.writeText(txtarea.Text);
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                //TextBox 
            }
        }
    }
}
