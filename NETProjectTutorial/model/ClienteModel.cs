﻿using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.model
{
    class ClienteModel
    {
        private static List<Cliente> clientes = new List<Cliente>();
        private Implement.DaoImplementsCliente daocliente;

        public ClienteModel()
        {

            daocliente = new Implement.DaoImplementsCliente();
        }

        public  List<Cliente> GetListClientes()
        {
            return daocliente.findAll();
        }

        public  void Populate()
        {
            Cliente[] pdts =
            {
                new Cliente(1,"0010888800", "Ana","paula","00000","Prueba","Prueba direccion")
        };


            foreach (Cliente c in clientes)
            {
                daocliente.save(c);
            }
            clientes = pdts.ToList();

        }
    }
}
