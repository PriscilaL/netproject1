﻿using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmFactura : Form
    {

        private DataSet dsSistema;
        private BindingSource bsProductoFactura;
        private bool _canUpdate = true;
        private bool _needUpdate = false;
        private double subtotal;
        private double iva;
        private double total;
        private string cod_factura;

        public FrmFactura()
        {
            InitializeComponent();
            bsProductoFactura = new BindingSource();
        }

        public DataSet DsSistema
        {
            set
            {
                dsSistema = value;
            }
        }

        private void FrmFactura_Load(object sender, EventArgs e)
        {
            cmbEmpleados.DataSource = dsSistema.Tables["Empleado"];
            cmbEmpleados.DisplayMember = "NA";
            cmbEmpleados.ValueMember = "Id";

            cmbProductos.DataSource = dsSistema.Tables["Producto"];
            cmbProductos.DisplayMember = "Nombre";
            cmbProductos.ValueMember = "Id";

            dsSistema.Tables["ProductoFactura"].Rows.Clear();
            bsProductoFactura.DataSource = dsSistema.Tables["ProductoFactura"];
            dgvProductoFactura.DataSource = bsProductoFactura;

            cod_factura = "FA" + dsSistema.Tables["Factura"].Rows.Count + 1;
            txtcodigo.Text = cod_factura;
        }

        private void cmbProductos_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataRow drProduto = ((DataRowView)cmbProductos.SelectedItem).Row;
            txtCantidad.Text = drProduto["Cantidad"].ToString();
            txtPrecio.Text = drProduto["Precio"].ToString();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                DataRow drProduto = ((DataRowView)cmbProductos.SelectedItem).Row;
                DataRow drProductoFactura = dsSistema.Tables["ProductoFactura"].NewRow();
                drProductoFactura["Id"] = drProduto["Id"];
                drProductoFactura["SKU"] = drProduto["SKU"];
                drProductoFactura["Nombre"] = drProduto["Nombre"];
                drProductoFactura["Cantidad"] = 1;
                drProductoFactura["Precio"] = drProduto["Precio"];
                dsSistema.Tables["ProductoFactura"].Rows.Add(drProductoFactura);
            }
            catch (ConstraintException)
            {
                MessageBox.Show(this, "Error, Producto existente", "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void dgvProductoFactura_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow dgrProductoFactura = dgvProductoFactura.Rows[e.RowIndex];
            DataRow drProductoFactura = ((DataRowView)dgrProductoFactura.DataBoundItem).Row;

            DataRow drProducto = dsSistema.Tables["Producto"].Rows.Find(drProductoFactura["Id"]);

            if (Int32.Parse(drProductoFactura["Cantidad"].ToString()) >
                Int32.Parse(drProducto["Caantidad"].ToString()))
            {
                MessageBox.Show(this, "ERROR en la cantidad de producto a vender no puede ser mayor que la del almacen", "Mensaje de Error",MessageBoxButtons.OK, MessageBoxIcon.Error);
                drProductoFactura["Cantidad"] = Int32.Parse(drProducto["Cantidad"].ToString());
            }

            CalcularTotalFactura();
        }

        private void CalcularTotalFactura()
        {
            subtotal = 0;
            foreach(DataRow dr in dsSistema.Tables["ProductoFactura"].Rows)
            {
                subtotal += Int32.Parse(dr["Cantidad"].ToString()) * Double.Parse(dr["Precio"].ToString());
            }
            iva = subtotal * 0.15;
            total = iva + subtotal;
        }

        private void dgvProductoFactura_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            CalcularTotalFactura();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection dgrProductoFactura = dgvProductoFactura.SelectedRows;
            if (dgrProductoFactura.Count == 0)
            {
                MessageBox.Show(this, "Error, no hay filas que eliminar", "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DataRow drProductoFactura = ((DataRowView)dgrProductoFactura[0].DataBoundItem).Row;
            dsSistema.Tables["ProductoFactura"].Rows.Remove(drProductoFactura);
            CalcularTotalFactura();
        }

        private void UpdateData()
        {
            if (cmbEmpleados.Text.Length > 1)
            {

                List<Empleado> searchData = dsSistema.Tables["Empleado"].AsEnumerable().Select(
                    dataRow =>
                    new Empleado
                    {
                        Id = dataRow.Field<Int32>("Id"),
                        Nombres = dataRow.Field<String>("Nombres"),
                        Apellidos = dataRow.Field<String>("Apellidos")
                    }).ToList();

                HandleTextChanged(searchData.FindAll(e => e.Nombres.Contains(cmbEmpleados.Text)));
            }
            else
            {
                RestartTimer();
            }
        }


        //Actualizar el combo con nuevos datos
        private void HandleTextChanged(List<Empleado> dataSource)
        {
            var text = cmbEmpleados.Text;

            if (dataSource.Count() > 0)
            {
                cmbEmpleados.DataSource = dataSource;

                var sText = cmbEmpleados.Items[0].ToString();
                cmbEmpleados.SelectionStart = text.Length;
                cmbEmpleados.SelectionLength = sText.Length - text.Length;
                cmbEmpleados.DroppedDown = true;
                return;
            }
            else
            {
                cmbEmpleados.DroppedDown = false;
                cmbEmpleados.SelectionStart = text.Length;
            }
        }

        private void RestartTimer()
        {
            timer1.Stop();
            _canUpdate = false;
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            _canUpdate = true;
            timer1.Stop();
            UpdateData();

        }

        private void cmbEmpleados_TextChanged(object sender, EventArgs e)
        {
            if (_needUpdate)
            {
                if (_canUpdate)
                {
                    _canUpdate = false;
                    UpdateData();
                }
                else
                {
                    RestartTimer();
                }
            }


        }

        private void cmbEmpleados_TextUpdate(object sender, EventArgs e)
        {
            _needUpdate = true;
        }

        private void cmbEmpleados_SelectedIndexChanged(object sender, EventArgs e)
        {
            _needUpdate = false;
        }

        private void btnFacturar_Click(object sender, EventArgs e)
        {
            if (dsSistema.Tables["ProductoFactura"].Rows.Count == 0) {
                MessageBox.Show(this, "Error, no se puede generar la factura, revise que hayan productos", "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DataRow drFactura = dsSistema.Tables["Factura"].NewRow();
            drFactura["Codfactura"] = cod_factura;
            drFactura["Fecha"] = DateTime.Now;
            drFactura["Observaciones"] = txtobser.Text;
            drFactura["Empleado"] = cmbEmpleados.SelectedValue;
            drFactura["Subtotal"] = subtotal;
            drFactura["Iva"] = iva;
            drFactura["Total"] = total;

            dsSistema.Tables["Factura"].Rows.Add(drFactura);

            foreach(DataRow dr in dsSistema.Tables["ProductoFactura"].Rows)
            {
                DataRow drDetallefactura = dsSistema.Tables["Detallefactura"].NewRow();
                drDetallefactura["Factura"] = drFactura["Id"];
                drDetallefactura["Producto"] = drFactura["Id"];
                drDetallefactura["Cantidad"] = dr["Cantidad"];
                drDetallefactura["Precio"] = dr["Precio"];
                dsSistema.Tables["DetalleFactura"].Rows.Add(drDetallefactura);

                DataRow drProducto = dsSistema.Tables["Producto"].Rows.Find(dr["Id"]);
                drProducto["Cantidad"] = Double.Parse(drProducto["Cantidad"].ToString()) - Double.Parse(drProducto["Cantidad"].ToString());
            }

            FrmReporteFactura frf = new FrmReporteFactura();
            frf.MdiParent = this.MdiParent;
            frf.DsSistema = dsSistema;
            frf.Show();

            Dispose();
        }
    }
}
