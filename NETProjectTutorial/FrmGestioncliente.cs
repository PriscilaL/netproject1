﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmGestioncliente : Form
    {
        private DataSet dsCliente;
        private BindingSource bsCliente;

        public DataSet DsCliente { set { dsCliente = value; } }

       

        public FrmGestioncliente()
        {
            InitializeComponent();
            bsCliente = new BindingSource();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            FrmCliente fc = new FrmCliente();
            fc.TblCliente = dsCliente.Tables["Cliente"];
            fc.DsCliente = dsCliente;
            fc.ShowDialog();
        }

        private void FrmGestioncliente_Load(object sender, EventArgs e)
        {
            bsCliente.DataSource = dsCliente;
            bsCliente.DataMember = dsCliente.Tables["Producto"].TableName;
            dataGridView1.DataSource = bsCliente;
            dataGridView1.AutoGenerateColumns = true;
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dataGridView1.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "ERROR, debe seleccionar una fila de la tabla para poder editar", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;

            FrmCliente fc = new FrmCliente();
            fc.TblCliente = dsCliente.Tables["Cliente"];
            fc.DsCliente = dsCliente;
            fc.DrCliente = drow;
            fc.ShowDialog();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {

            DataGridViewSelectedRowCollection rowCollection = dataGridView1.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "ERROR, debe seleccionar una fila de la tabla para poder editar", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;

            DialogResult result = MessageBox.Show(this, "Realmente desea eliminar ese registro?", "Mensaje del Sistema", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                //DsCliente.Tables["Cliente"].Rows.Remove(drow);
                MessageBox.Show(this, "Registro eliminado satisfactoriamente!", "Mensaje del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }
    }
}
