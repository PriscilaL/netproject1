﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmCliente : Form
    {
        private DataTable tblCliente;
        private DataSet dsProductos;
        private BindingSource bsProductos;
        private DataRow drCliente;
        public FrmCliente()
        {
            InitializeComponent();
            bsProductos = new BindingSource();
        }

        public DataTable TblCliente { set { tblCliente = value; } }
        public DataSet DsCliente { set { dsProductos = value; } }

        public DataRow DrCliente
        {
            set
            {
                drCliente = value;
                txtCedula.Text = drCliente["Cedula"].ToString();
                txtNombre.Text = drCliente["Nombres"].ToString();
                txtApellido.Text = drCliente["Apellidos"].ToString();
                txtTel.Text = drCliente["Telefono"].ToString();
                txtCorreo.Text = drCliente["Correo"].ToString();
                txtDire.Text = drCliente["Direccion"].ToString();
            }

        }
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            string cedula, nombre,apellido,telefono,correo,direccion;
            cedula = txtCedula.Text;
            nombre = txtNombre.Text;
            apellido = txtApellido.Text;
            telefono = txtTel.Text;
            correo = txtCorreo.Text;
            direccion = txtDire.Text;

            if(drCliente!= null)
            {
                DataRow drNew = tblCliente.NewRow();

                int index = tblCliente.Rows.IndexOf(drCliente);
                drNew["ID"] = drCliente["ID"];
                drNew["Cedula"] = cedula;
                drNew["Nombres"] = nombre;
                drNew["Apellidos"] = apellido;
                drNew["Telefono"] = telefono;
                drNew["Correo"] = correo;
                drNew["Direccion"] = direccion;

                tblCliente.Rows.RemoveAt(index);
                tblCliente.Rows.InsertAt(drNew, index);
            }
            else
            {
                tblCliente.Rows.Add(tblCliente.Rows.Count + 1, cedula,nombre,apellido,telefono,correo,direccion);
            }

            Dispose();

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void FrmCliente_Load(object sender, EventArgs e)
        {
            bsProductos.DataSource = dsProductos;
            bsProductos.DataMember = dsProductos.Tables["Cliente"].TableName;
        }
    }
}
