﻿using NETProjectTutorial.entities;
using NETProjectTutorial.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmMain : Form
    {
        private DataTable dtProductos;
       

        public FrmMain()
        {
            InitializeComponent();
        }

        private void productosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionProductos fgp = new FrmGestionProductos();
            fgp.MdiParent = this;
            fgp.DsProductos = dsProductos;
            fgp.Show();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            dtProductos = dsProductos.Tables["Producto"];
            ProductoModel.Populate();
            foreach (Producto p in ProductoModel.GetProductos())
            {
                dtProductos.Rows.Add(p.Id, p.Sku, p.Nombre, p.Descripcion, p.Cantidad, p.Precio);
            }

            EmpleadoModel.Populate();
            foreach (Empleado emp in EmpleadoModel.GetEmpleados())
            {
                dsProductos.Tables["Empleado"].Rows.Add(emp.Id, emp.Id, emp.Cedula, emp.Nombres, emp.Apellidos, 
                emp.Direccion, emp.Tconvensional, emp.Tcecular, emp.Sexo, emp.Salario, emp.Nombres+""+emp.Apellidos);
            }

            //new ClienteModel().Populate();
            foreach (Cliente cli in new ClienteModel().GetListClientes())
            {
                dsProductos.Tables["Cliente"].Rows.Add(cli.Id, cli.Ced, cli.Nombre, cli.Apellido, cli.Tel, cli.Correo, cli.Dir);
            }

        }

        private void empleadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionEmpleados fge = new FrmGestionEmpleados();
            fge.MdiParent = this;
            fge.DsEmpleados = dsProductos;
            fge.Show();
        }

        private void facturaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmFactura ff = new FrmFactura();
            ff.MdiParent = this;
            ff.DsSistema = dsProductos;
            ff.Show();
        }

        private void generarFacturaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Ventana v = new Ventana();
            v.MdiParent = this;
            v.Show();
        }

        private void ClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestioncliente fgc = new FrmGestioncliente();
            fgc.MdiParent = this;
            fgc.DsCliente = dsProductos;
            fgc.Show();
        }
    }
}
