﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class ReporteFactura
    {
        //Factura
        private string cod_factura;
        private DateTime fecha;
        private double subtotal;
        private double iva;
        private double total;
        //DetalleFactura
        private int cantidad;
        private double precio;
        //Producto
        private string sku;
        private string nombre_Producto;
        //Empleado
        private string nombre_Empleado;
        private string apellido_Empleado;

        public string Cod_factura
        {
            get
            {
                return cod_factura;
            }

            set
            {
                cod_factura = value;
            }
        }

        public DateTime Fecha
        {
            get
            {
                return fecha;
            }

            set
            {
                fecha = value;
            }
        }

        public double Subtotal
        {
            get
            {
                return subtotal;
            }

            set
            {
                subtotal = value;
            }
        }

        public double Iva
        {
            get
            {
                return iva;
            }

            set
            {
                iva = value;
            }
        }

        public double Total
        {
            get
            {
                return total;
            }

            set
            {
                total = value;
            }
        }

        public int Cantidad
        {
            get
            {
                return cantidad;
            }

            set
            {
                cantidad = value;
            }
        }

        public double Precio
        {
            get
            {
                return precio;
            }

            set
            {
                precio = value;
            }
        }

        public string Sku
        {
            get
            {
                return sku;
            }

            set
            {
                sku = value;
            }
        }

        public string Nombre_Producto
        {
            get
            {
                return nombre_Producto;
            }

            set
            {
                nombre_Producto = value;
            }
        }

        public string Nombre_Empleado
        {
            get
            {
                return nombre_Empleado;
            }

            set
            {
                nombre_Empleado = value;
            }
        }

        public string Apellido_Empleado
        {
            get
            {
                return apellido_Empleado;
            }

            set
            {
                apellido_Empleado = value;
            }
        }

        public ReporteFactura(string cod_factura, DateTime fecha, double subtotal, double iva, double total, int cantidad, double precio, string sku, string nombre_Producto, string nombre_Empleado, string apellido_Empleado)
        {
            this.Cod_factura = cod_factura;
            this.Fecha = fecha;
            this.Subtotal = subtotal;
            this.Iva = iva;
            this.Total = total;
            this.Cantidad = cantidad;
            this.Precio = precio;
            this.Sku = sku;
            this.Nombre_Producto = nombre_Producto;
            this.Nombre_Empleado = nombre_Empleado;
            this.Apellido_Empleado = apellido_Empleado;
        }
    }
}
