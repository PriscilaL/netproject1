﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class Cliente
    {

        int id;//4
        string ced;//16=>35

        string nombre;//20=>43
        string apellido;//20=>43
        string tel;//8=>19
        string correo;//20=43
        string dir;//100=>203
        //total=>390

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Ced
        {
            get
            {
                return ced;
            }

            set
            {
                ced = value;
            }
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public string Apellido
        {
            get
            {
                return apellido;
            }

            set
            {
                apellido = value;
            }
        }

        public string Tel
        {
            get
            {
                return tel;
            }

            set
            {
                tel = value;
            }
        }

        public string Correo
        {
            get
            {
                return correo;
            }

            set
            {
                correo = value;
            }
        }

        public string Dir
        {
            get
            {
                return dir;
            }

            set
            {
                dir = value;
            }
        }

        public Cliente(int id, string ced, string nombre, string apellido, string tel, string correo, string dir)
        {
            this.Id = id;
            this.Ced = ced;
            this.Nombre = nombre;
            this.Apellido = apellido;
            this.Tel = tel;
            this.Correo = correo;
            this.Dir = dir;
        }
    }
}
