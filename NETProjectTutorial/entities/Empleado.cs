﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class Empleado
    {
        int id;
        private string inss;
        private string cedula;
        private string nombres;
        private string apellidos;
        private string direccion;
        private string tconvencional;
        private string tcecular;
        private SEXO sexo;
        private double salario;

        public Empleado(){

        }

        public Empleado(int id, string inss, string cedula, string nombres, string apellidos, string direccion, string tconvencional, string tcecular, SEXO sexo, double salario)
        {
            this.Id = id;
            this.Inss = inss;
            this.Cedula = cedula;
            this.Nombres = nombres;
            this.Apellidos = apellidos;
            this.Direccion = direccion;
            this.Tconvensional = tconvencional;
            this.Tcecular = tcecular;
            this.Sexo = sexo;
            this.Salario = salario;
        }

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Inss
        {
            get
            {
                return inss;
            }

            set
            {
                inss = value;
            }
        }

        public string Cedula
        {
            get
            {
                return cedula;
            }

            set
            {
                cedula = value;
            }
        }

        public string Nombres
        {
            get
            {
                return nombres;
            }

            set
            {
                nombres = value;
            }
        }

        public string Apellidos
        {
            get
            {
                return apellidos;
            }

            set
            {
                apellidos = value;
            }
        }

        public string Direccion
        {
            get
            {
                return direccion;
            }

            set
            {
                direccion = value;
            }
        }

        public string Tconvensional
        {
            get
            {
                return tconvencional;
            }

            set
            {
                tconvencional = value;
            }
        }

        public string Tcecular
        {
            get
            {
                return tcecular;
            }

            set
            {
                tcecular = value;
            }
        }

        public SEXO Sexo
        {
            get
            {
                return sexo;
            }

            set
            {
                sexo = value;
            }
        }

        public double Salario
        {
            get
            {
                return salario;
            }

            set
            {
                salario = value;
            }
        }

        public enum SEXO
        {
            Female, Male
        }

        public override string ToString()
        {
            return Nombres + "" + Apellidos;
        }
    }

    
}
